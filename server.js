
const express = require("express"); 
const bodyParser = require('body-parser');
const astronauts = require('./bdd').astronauts;


const app = express();

//Body Parser
const urlencodedParser = bodyParser.urlencoded({
    extended: true
});
app.use(urlencodedParser);
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});  


app.get('/astronauts',function(req,res){
    res.json({astronauts: astronauts})
})

app.get('/astronauts/:name',function(req,res){
    const name = req.params.name;
    const response = [];
    const foundedAstronats = astronauts.find(element => element.name === name);
    console.log(foundedAstronats)

    if(!foundedAstronats){
        console.log('Astronaut: Not found')
        res.json({astronauts: response})
        return
    }

    response.push(foundedAstronats);
    res.json({astronauts: response})
})

app.get('/astronaut/:id',function(req,res){
    const id = req.params.id;
    const foundedAstronaut = astronauts.find(element => element.id == id);

    if(!foundedAstronaut){
        console.log('Astronaut: Not found');
        return;
    }
    console.log(foundedAstronaut)
    res.json({astronaut: foundedAstronaut})
})

app.post('/astronaut',function(req,res){
    const id = parseInt(astronauts[astronauts.length-1].id) + 1;
    const object = 
    {
        id,
        name: `astronaut ${id}`,
        avatar_url: '/astronauts.jpg'
    };
    astronauts.push(object);
    res.json({astronauts: astronauts})
})

app.delete('/:id', function(req, res){
    const param  = req.params.id;
    for(let element of astronauts){
        if(param === element.id){
            astronauts.splice(astronauts.indexOf(element), 1);
            console.log('Astronaut has been deleted')
            return;
        }
    }
});

app.listen(8080, () => console.log(`Listening on port ${8080}`));