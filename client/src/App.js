import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Astronauts from './components/Astronauts';
import AstronautItem from './components/astronautItem';
import Navbar from './components/partials/Navbar';


function App() {
  return (
    <Router>
      <div className="App">
          <Navbar />
        <div className="container mt-2">
          <Switch>
            <Route exact path="/:id" component={AstronautItem} />
            <Route exact component={Astronauts} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
