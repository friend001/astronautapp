import React, { Component } from 'react'

export class SearchAstronaut extends Component {

    state = {
        search: ''
    }

    handleFrom = (e) => {
        
        this.setState ({
            search: e.target.value
        })
    }

    searchAstronaut = (e) => {
        e.preventDefault();
        this.props.getSearchedAstronaut(this.state.search)
    }

    render() {
        const {search} = this.state;
        return (
            <form onSubmit={this.searchAstronaut}> 
                <div className="form-group">
                    <input onChange={this.handleFrom} value={search} placeholder="Search astronauts..." id="search" type="text" className="form-control"/>
                </div>
                <button className="btn btn-primary btn-block">Search</button>
            </form>
            )
        }
}

export default SearchAstronaut
