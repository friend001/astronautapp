import React, { Component } from 'react';
import Astronaut from './Astronaut';
import axios from 'axios';



class astronautItem extends Component {

    state = {
        astronaut: {}
    }

    componentWillMount(){
        const id = this.props.match.params.id;

        axios.get(`http://localhost:8080/astronaut/${id}`)
        .then( (response) => {
            // handle success
            console.log(response.data);
            this.setState({
                astronaut: response.data.astronaut
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }

    deleteAstronaut = () => {
        console.log('1111', this.state.astronaut)    
        axios.delete(`http://localhost:8080/${this.state.astronaut.id}`)
        .then( () => {
            // handle success
            console.log('Astronaut has been deleted');
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }

    render() {
        return (
            <div>
               <a href='/'> <button type="button" className="btn btn-outline-primary mt-4" onClick={this.deleteAstronaut}>Delete Astronaut</button></a> 
                <Astronaut astronaut={this.state.astronaut} />
            </div>
        )
    }
}

export default astronautItem
