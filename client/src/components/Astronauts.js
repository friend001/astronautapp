import React, { Component } from 'react'
import Astronaut from './Astronaut';
import SearchAstronaut from './SearchAstronaut';
import axios from 'axios';

export class Astronauts extends Component {
    
    state = {
        astronauts : [
            {
                id: '1',
                name: 'default',
                avatar_url: '/astronauts.jpg'
            }
        ]    
    }

    getAstronauts = () => {    
        axios.get('http://localhost:8080/astronauts')
        .then( (response) => {
            // handle success
            console.log(response.data);
            this.setState({
                astronauts: response.data.astronauts
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }

    addAstronauts = () => {    
        axios.post('http://localhost:8080/astronaut')
        .then( (response) => {
            // handle success
            console.log(response.data);
            this.setState({
                astronauts: response.data.astronauts
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }

    componentDidMount() {
        this.getAstronauts();
    }

    searchAstronaut = (data) => {
       axios.get(`http://localhost:8080/astronauts/${data}`)
        .then( (response) => {
            // handle success
            console.log(response.data);
            this.setState({
                astronauts: response.data.astronauts
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }
    
    render() {
        return (
            <div>
                <div className="row my-2">
                    <div className="col-md-12">
                        <SearchAstronaut getSearchedAstronaut={this.searchAstronaut}/>
                    </div>
                </div>
                <button type="button" className="btn btn-outline-primary mt-4" onClick={this.addAstronauts}>Generate new Astronaut</button>
                <div className="row">
                {this.state.astronauts.map(astronaut => {
                return <Astronaut astronaut={astronaut} />
                })}
                </div>
            </div>
        )
    }
}

export default Astronauts;
