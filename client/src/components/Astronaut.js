import React, { Component } from 'react'
import avatar from './astronauts.jpg'

export class Astronaut extends Component {
    render() {
        const {id, name} = this.props.astronaut;
        return (
            
            <div className="col-md-4 mt-4" key={id}>
                <div className="card">
                    <img className="card-img-top" src={avatar} alt="" />
                    <div className="card-body">
                        <h4 className="card-title">{name}</h4>
                        <p className="card-text">
                            <a className="btn btn-primary" href={"/"+id}> id number : {id} </a>
                        </p>
                    </div>
                </div>
            </div>
            
        )
    }
}

export default Astronaut
